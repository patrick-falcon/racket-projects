#lang racket

(define my-third (λ (x) (list-ref x 2)))
(define my-last (λ (x) (list-ref x (- (length x) 1))))

(define fill(λ(x)

              (cond
                ((positive? (length x)) (cons (length x) x))
                ((zero? (length x)) "Empty List")
                )))

(define LHRest(λ (x) (reverse (rest (reverse x)))))

(define x '(a b c d e f))
(define empty'())

(my-third x)
(my-last x)
(fill x)
(LHRest x)
